const iconTop = document.querySelector(`.top`);
const iconBottom = document.querySelector(`.bottom`);
iconTop.addEventListener(`click`, function(){
    let input = document.getElementById(`password-top`)
    if(input.getAttribute(`type`) === `password`){
    iconTop.setAttribute(`class`, `fas fa-eye-slash icon-password top`);
    input.setAttribute(`type`, `text`)
}else{
    iconTop.setAttribute(`class`, `fas fa-eye icon-password top`);
    input.setAttribute(`type`, `password`);
};
});

iconBottom.addEventListener(`click`, function(){
    let input = document.getElementById(`password-bottom`)
    if(input.getAttribute(`type`) === `password`){
    iconBottom.setAttribute(`class`, `fas fa-eye-slash icon-password bottom`);
    input.setAttribute(`type`, `text`)
}else{
    iconBottom.setAttribute(`class`, `fas fa-eye icon-password bottom`);
    input.setAttribute(`type`, `password`);
};
});

const submitButton = document.querySelector(`.btn`);
submitButton.addEventListener(`click`, function(){
    let topPassword = document.getElementById(`password-top`);
    let bottomPassword = document.getElementById(`password-bottom`);
    let bottomText = document.querySelector(`.text`);
    if (topPassword.value === bottomPassword.value){
        bottomText.style.display = `none`;
        alert(`You are welcome`);
    }else{
        bottomText.style.display = `block`;
    }
});

