/*Опишите своими словами как работает цикл forEach. */
// Это метод который позволяет достучатся до каждого элемента в массиве, перебрать их и к примеру вывести каджый из них в консоль и т.д.

function filteredBy(filteredArray, dataType){
    const filterResult = filteredArray.filter(element => typeof element !== dataType);
    return filterResult;
}
console.log(filteredBy(['he11o', 'world', 23, '23', null,], 'string'));