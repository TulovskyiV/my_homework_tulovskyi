/*Почему для работы с input не рекомендуется использовать события клавиатуры?*/
//К примеру пользователь может ввести данные не используя клавиатуру, типа клик мышкой "вставить" или же он сидит с телефона или планшета, намного лучше использывать ивенты типа change и т.д.


const btnEnter = document.getElementById(`enter`);
const btnS = document.getElementById(`s`);
const btnE = document.getElementById(`e`);
const btnO = document.getElementById(`o`);
const btnN = document.getElementById(`n`);
const btnL = document.getElementById(`l`);
const btnZ = document.getElementById(`z`);
const btn = document.querySelectorAll(`.btn`);


function lightButton(){
    window.addEventListener(`keydown`, function (e) {
        if(e.key === `Enter`){
            btn.forEach(element =>{
                element.classList.remove(`active`);
            });
            btnEnter.classList.add(`active`);
            
        }
        if(e.key === `s` || e.key === `S`){
            btn.forEach(element =>{
                element.classList.remove(`active`);
            });
            btnS.classList.add(`active`);
        }
        if(e.key === `e`|| e.key === `E`){
            btn.forEach(element =>{
                element.classList.remove(`active`);
            });
            btnE.classList.add(`active`);
        }
        if(e.key === `o`|| e.key === `O`){
            btn.forEach(element =>{
                element.classList.remove(`active`);
            });
            btnO.classList.add(`active`);
        }
        if(e.key === `n`|| e.key === `N`){
            btn.forEach(element =>{
                element.classList.remove(`active`);
            });
            btnN.classList.add(`active`);
        }
        if(e.key === `l`|| e.key === `L`){
            btn.forEach(element =>{
                element.classList.remove(`active`);
            });
            btnL.classList.add(`active`);
        }
        if(e.key === `z`|| e.key === `Z`){
            btn.forEach(element =>{
                element.classList.remove(`active`);
            });
            btnZ.classList.add(`active`);
        };
    });
};
lightButton();