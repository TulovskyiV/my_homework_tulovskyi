const tab = function(){
    let tabTitle = document.querySelectorAll(`.tabs-title`);
    let tabContent = document.querySelectorAll(`.tab`);

    tabTitle.forEach(element =>{
        element.addEventListener(`click`, selectTab)
    });
    function selectTab(){
        tabTitle.forEach(element =>{
            element.classList.remove(`active`);
        });
        this.classList.add(`active`);
        tabName = this.getAttribute(`data-title`);
        showTabContent(tabName);
    }
    function showTabContent(tabName){
        tabContent.forEach(element =>{
            element.classList.contains(tabName) ? element.classList.add(`active`) : element.classList.remove (`active`);
        })
    }

};

tab();

