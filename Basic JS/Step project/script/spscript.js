function showOurServices(){
    const tabsTitle = document.querySelectorAll(`.tabs-title`);
    const tabsContent = document.querySelectorAll(`.tab`);

        for (let i = 0; i < tabsTitle.length; i++){
            tabsTitle[i].addEventListener(`click`, function(e){
            let activeTab = e.target.getAttribute(`data-title`);

        for (let j = 0; j < tabsTitle.length; j++){
            let contentAttr = tabsContent[j].getAttribute(`data-content`);

            if (activeTab === contentAttr){
                tabsTitle[j].classList.add(`active`);
                tabsContent[j].classList.add(`active`);
            }else{
                tabsTitle[j].classList.remove(`active`);
                tabsContent[j].classList.remove(`active`);
            };
        };
    });
};
};
showOurServices();

function loadMore(){
    const button = document.querySelector(`.load-more`);
    button.addEventListener(`click`, function() {
    let showMore = document.querySelectorAll(`.more-items`);
    showMore.forEach(item => {
    item.classList.remove(`more-items`)
    button.style.display = `none`;
    })
    });
};
loadMore();


function showAmazingWork(){
    const buttons = document.querySelectorAll(`.amazing-menu-item`);
    const pictures = document.querySelectorAll(`.amazing-item-image`);

    function filtration (category, items) {
        items.forEach((item) => {
            const itemAttr = item.getAttribute(`data-filter`);
            const filteredItem = itemAttr !== category;
            const showAll = category === `all`;
            if(filteredItem && !showAll){
                item.classList.add(`hide`);
            }else{
                item.classList.remove(`hide`);
            }
        })
    };

    buttons.forEach((button) => {
        button.addEventListener(`click`, () => {
            filtration(button.dataset.filter, pictures)
        })
    })
}
showAmazingWork();




const hideAuthors = document.getElementsByClassName(`hide-authors`);
const hideAuthorsMini = document.getElementsByClassName(`hide-authors-mini`);
const miniCirclePhoto = Array.from(document.getElementsByClassName(`mini-photo`));
const leftButton = document.getElementsByClassName(`left`);
const rightButton = document.getElementsByClassName(`right`);

const applyHiddenClass = (cls, length = cls.length, count = 0) => {
    for (let i = count; i < length; i++) {
        cls[i].style.display = `none`;
    }
};

applyHiddenClass(hideAuthors);
applyHiddenClass(hideAuthorsMini);






function showPeople() {
    const miniPhoto = Array.from(document.getElementsByClassName(`mini-photo`));
    const Peoplequote = Array.from(document.getElementsByClassName(`authors-main-block`));
    Peoplequote.forEach((value) => value.style.display = `none`);
    const dataPeople = this.getAttribute(`data-people`);
    const quoteData = Peoplequote.filter((value) => {
        return value.getAttribute(`data-people`) === dataPeople;
    });
    quoteData[0].style.display = ``;
    miniPhoto.forEach((value) => value.classList.remove(`mini-circle-border`));
    this.classList.add(`mini-circle-border`);
}
miniCirclePhoto.forEach((value) => value.addEventListener(`click`, showPeople));

const slideLeft = () => {
    const miniImages = Array.from(document.getElementsByClassName(`mini-photo`));
    const currentImageIndex = miniImages.findIndex((items) => items.classList.contains(`mini-circle-border`));
    const currentImage = document.getElementsByClassName(`mini-circle-border`);
    let prevImageIndex = currentImageIndex - 1;
    let prevImage = miniImages[prevImageIndex];
    const bigPeopleBlock = Array.from(document.getElementsByClassName(`authors-main-block`));

    bigPeopleBlock.forEach((items) => items.style.display = `none`);

    if (currentImageIndex === 0) {
        miniImages.forEach((items) => items.style.display = ``);
        prevImage = miniImages[miniImages.length - 1];
        prevImageIndex = miniImages.length - 1;
        currentImage[currentImageIndex].style.display = `none`;
    }
    if (prevImage.style.display === `none`) {
        prevImage.style.display = ``;
        miniImages[currentImageIndex + 3].style.display = `none`;
    }
    currentImage[0].classList.remove(`mini-circle-border`);
    prevImage.classList.add(`mini-circle-border`);
    bigPeopleBlock[prevImageIndex].style.display = ``;
};

const slideRight = () => {
    const miniImages = Array.from(document.getElementsByClassName(`mini-photo`));
    const currentImageIndex = miniImages.findIndex((items) => items.classList.contains(`mini-circle-border`));
    const currentImage = document.getElementsByClassName(`mini-circle-border`);
    let nextImageIndex = currentImageIndex + 1;
    let nextImage = miniImages[nextImageIndex];
    const bigPeopleBlock = Array.from(document.getElementsByClassName(`authors-main-block`));

    bigPeopleBlock.forEach((items) => items.style.display = `none`);

    if (currentImageIndex === (miniImages.length - 1)) {
        miniImages.forEach((items) => items.style.display = ``);
        nextImage = miniImages[0];
        nextImageIndex = 0;
        currentImage[0].style.display = `none`;
    }
    if (nextImage.style.display === `none`) {
        nextImage.style.display = ``;
        miniImages[currentImageIndex - 3].style.display = `none`;
    }
    currentImage[0].classList.remove(`mini-circle-border`);
    nextImage.classList.add(`mini-circle-border`);
    bigPeopleBlock[nextImageIndex].style.display = ``;
};



leftButton[0].addEventListener(`click`, slideLeft);
rightButton[0].addEventListener(`click`, slideRight);