
const darkStyle = document.createElement(`link`);
darkStyle.setAttribute(`rel`,`stylesheet`);
darkStyle.setAttribute(`href`,`./CSS/darktheme.css`);

const darkThemeLink = document.getElementById(`dark theme`);
darkThemeLink.addEventListener(`click`, function themeSetterDark(){
    localStorage.setItem(`theme`, `black`);
    let theme = localStorage.getItem(`theme`);
    console.log(theme)
    if(theme === `black`){
        document.head.append(darkStyle);
    };
});

const lightThemeLink = document.getElementById(`light theme`);
lightThemeLink.addEventListener(`click`, function themeSetterLight(){
    localStorage.setItem(`theme`, `light`);
    let theme = localStorage.getItem(`theme`);
    console.log(theme)
    if(theme === `light`){
        darkStyle.remove();
    };
}); 

const theme = localStorage.getItem(`theme`);
if(theme === `light`){
    darkStyle.remove();
}else{
    document.head.append(darkStyle);
};

