// 1 variant - global constructor Object
// const product = new Object()

// 2 variant - function constructor


//3 variant - object literal
const product = {
    title: "very good skills",
    cost: 500,
    "expiration date": "forever",
    getExpirationDate() {
        return product["expiration date"];
    }
};

// const product2 = product;

// for (const key in product) {
//       product2[key] = product[key];
// }


// const product2 = Object.assign({}, product);


// product2.title = "Super good sills";

// console.log(product2.title);
// console.log(product.title);



// console.log(Object.assign({name: 1, title: "obj"},{Name: 2, date: "2021"}));

// console.log(Object.keys(product));
// console.log(Object.values(product));
// console.log(Object.entries(product));
// console.log(product.hasOwnProperty("title"));

Object.defineProperty(product, "title", {
    writable: false,
} )
