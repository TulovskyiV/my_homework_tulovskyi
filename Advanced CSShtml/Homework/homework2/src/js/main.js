function handleBurger() {
        document.querySelector('.burger').addEventListener('click', () => {
        document.querySelector('.menu').classList.toggle('active')
        document.querySelector(`.burger__menu`).classList.toggle(`cross`)
    })
}


window.addEventListener('DOMContentLoaded', () => {
    handleBurger()
})