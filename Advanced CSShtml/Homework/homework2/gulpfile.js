import gulp from 'gulp'
import dartSass from 'sass'
import gulpSass from 'gulp-sass'
import browserSync from 'browser-sync'
import minifyjs from 'gulp-js-minify'
import uglify from 'gulp-uglify'
import cleanCSS from 'gulp-clean-css'
import clean from 'gulp-clean'
import concat from 'gulp-concat'
import imagemin from 'gulp-imagemin'
import autoprefixer from 'gulp-autoprefixer'






const sass = gulpSass(dartSass)
browserSync.create()



const path = {
	src: {
		scss: './src/scss/**/*.scss',
		js: './src/js/*.js',
		img: './src/images/*',
	},
	dist: {
		self: './dist/',
		css: './dist/css/',
		js: './dist/js/',
		img: './dist/images/',
	},
}



const buildScss = () =>
	gulp
		.src(path.src.scss)
		.pipe(sass().on('error', sass.logError))
		.pipe(
			autoprefixer({
				cascade: false,
			}),
		)
		.pipe(gulp.dest(path.dist.css))
		.pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(browserSync.stream())

const buildJs = () => gulp.src(path.src.js).pipe(concat('main.js')).pipe(uglify()).pipe(minifyjs()).pipe(gulp.dest(path.dist.js)).pipe(browserSync.stream())

const buildImgs = () => gulp.src(path.src.img).pipe(imagemin()).pipe(gulp.dest(path.dist.img))

const watcher = () => {
	browserSync.init({
		server: {
			baseDir: './',
		},
	})

	gulp.watch('./index.html').on('change', browserSync.reload)
	gulp.watch(path.src.scss, buildScss).on('change', browserSync.reload)
	gulp.watch(path.src.js, buildJs).on('change', browserSync.reload)
	gulp.watch(path.src.img, buildImgs).on('change', browserSync.reload)
}

const cleanBuild = () => gulp.src(path.dist.self, { allowEmpty: true }).pipe(clean())



const build = gulp.series(buildScss, buildJs)

gulp.task('dev', gulp.series(cleanBuild, gulp.parallel(buildImgs, build), watcher))
gulp.task('prod', gulp.series(cleanBuild, gulp.parallel(buildImgs, build), watcher))


