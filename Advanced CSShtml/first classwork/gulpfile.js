const gulp = require(`gulp`),
    concat = require('gulp-concat');

// gulp.task(`buildHtml`, () =>(
//     gulp
//         .src(`./src/index.html`)
//         .pipe(gulp.dest(`./prod`))
// ))

/********** PATHS **********/

const path = {
    src:{
        css:`./src/css/*.css`,
        js:`./src/js/*.js`
    },
    prod:{
        self:`./prod`,
        js:`./src/prod/*.js`,
        css:`./src/prod/*.css`
    }
};

/********** FUNCTIONS **********/

const buildCss = () => (
    gulp.src(path.src.css)
        .pipe(concat('main.css'))
        .pipe(gulp.dest(path.prod.css))
        .pipe(browserSync.stream())

)

const buildJs = () => (
    gulp.src(path.src.css)
        .pipe(concat('main.js'))
        .pipe(gulp.dest(path.prod.js))
        .pipe(browserSync.stream())

)

const wathcer = () => {
        browserSync.init({
            server:{
                baseDir: "./"
            }
        });

        gulp.watch(path.src.css).on(`change`, browserSync.reload)
        gulp.watch(path.src.js).on(`change`, browserSync.reload)
    }

/********** TASK **********/


gulp.task(`dev`, gulp.series(buildCss, buildJs))


// gulp.series
// gulp.parallel
